package com.alicorp.pe.heredemo

import android.graphics.PointF
import android.widget.Toast
import com.here.android.mpa.common.ViewObject
import com.here.android.mpa.mapping.MapGesture

class MyGestureListener(val activity: MapActivity): MapGesture.OnGestureListener {
    override fun onLongPressRelease() {

    }

    override fun onRotateEvent(p0: Float): Boolean {
        return false
    }

    override fun onMultiFingerManipulationStart() {

    }

    override fun onPinchLocked() {

    }

    override fun onPinchZoomEvent(p0: Float, p1: PointF): Boolean {
        return false
    }

    override fun onTapEvent(p0: PointF): Boolean {
        activity.runOnUiThread {
            Toast.makeText(activity, "Tap on map", Toast.LENGTH_SHORT).show()
        }
        return false
    }

    override fun onPanStart() {

    }

    override fun onMultiFingerManipulationEnd() {

    }

    override fun onDoubleTapEvent(p0: PointF): Boolean {
        activity.runOnUiThread {
            Toast.makeText(activity, "Double tap on map", Toast.LENGTH_SHORT).show()
        }
        return false
    }

    override fun onPanEnd() {

    }

    override fun onTiltEvent(p0: Float): Boolean {
        return false
    }

    override fun onMapObjectsSelected(p0: MutableList<ViewObject>): Boolean {
        return false
    }

    override fun onRotateLocked() {

    }

    override fun onLongPressEvent(p0: PointF): Boolean {
        return false
    }

    override fun onTwoFingerTapEvent(p0: PointF): Boolean {
        activity.runOnUiThread {
            Toast.makeText(activity, "Two finger tap on map", Toast.LENGTH_SHORT).show()
        }
        return false
    }
}