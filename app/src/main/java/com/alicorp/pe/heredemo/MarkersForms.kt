package com.alicorp.pe.heredemo


import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PointF
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import com.here.android.mpa.cluster.ClusterLayer
import com.here.android.mpa.common.*
import com.here.android.mpa.mapping.*
import com.here.android.mpa.mapping.Map
import java.util.*


class MarkersForms : AppCompatActivity() {

    private var m_mapFragment: AndroidXMapFragment? = null
    private var m_map: Map? = null
    private var  groupMarkerDetails: Group? = null
    private var container_details: ConstraintLayout? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_markers_forms)

        var toolbar = findViewById<Toolbar>(R.id.toolbar)

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Marcadores y Formas"

        m_mapFragment = supportFragmentManager.findFragmentById(R.id.mapfragment) as AndroidXMapFragment?
        groupMarkerDetails = findViewById(R.id.groupMarkerDetails)
        container_details = findViewById(R.id.container_details)
        initMap()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.markers_forms, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionMarkers -> showMarkers()
            R.id.actionPolyline -> showPolyLines()
            R.id.actionCluster -> showCluster()
        }
        return true
    }


    fun showCluster(){

        val cl = ClusterLayer()

        val icon = BitmapFactory.decodeResource(resources,
                R.drawable.location)

        val image = Image()
        image.setBitmap(Bitmap.createScaledBitmap(icon, 80, 80, false))


        var markerCoordinate01 =  GeoCoordinate(-12.097264, -77.023000);
        var marker01 = MapMarker(markerCoordinate01, image)

        var markerCoordinate02 =  GeoCoordinate(-12.097601, -77.022881, 10.0);
        var marker02 = MapMarker(markerCoordinate02, image)

        var markerCoordinate03 =  GeoCoordinate(-12.097478, -77.023126, 10.0);
        var marker03 = MapMarker(markerCoordinate03, image)

        var markerCoordinate04 =  GeoCoordinate(-12.097840, -77.022961, 10.0);
        var marker04 = MapMarker(markerCoordinate04, image)

        var markerCoordinate05 =  GeoCoordinate(-12.097563, -77.023272, 10.0);
        var marker05 = MapMarker(markerCoordinate05, image)

        var markerCoordinate06 =  GeoCoordinate(-12.097572, -77.023048, 10.0);
        var marker06 = MapMarker(markerCoordinate06, image)

        cl.addMarker(marker01)
        cl.addMarker(marker02)
        cl.addMarker(marker03)
        cl.addMarker(marker04)
        cl.addMarker(marker05)
        cl.addMarker(marker06)

        m_map?.addClusterLayer(cl);
    }

    fun showMarkers(){

        val icon = BitmapFactory.decodeResource(resources,
                R.drawable.location)

        val image = Image()
        image.setBitmap(Bitmap.createScaledBitmap(icon, 80, 80, false))


        var markerCoordinate =  GeoCoordinate(-12.087913, -77.022255, 10.0);
        var marker = MapMarker(markerCoordinate, image)
        m_map?.addMapObject(marker);


        var markerObj: CustomMarker = CustomMarker("Av. Paseo de la República 3440, San Isidro 15046", "Hiper plazaVea San Isidro", "+51948663808",-12.096893, -77.025667 )
        var markerCoordinate2 =  GeoCoordinate(markerObj.latitude, markerObj.longitude, 10.0);
        var marker2 = MapLabeledMarker(markerCoordinate2, image)
        marker2.setLabelText("spa", markerObj?.name)
        marker2.tag = markerObj
        m_map?.addMapObject(marker2);
    }

    fun showPolyLines(){
        val testPoints: MutableList<GeoCoordinate> = ArrayList()
        testPoints.add(GeoCoordinate(-12.088697, -77.020372, 10.0))
        testPoints.add(GeoCoordinate(-12.088477, -77.018699, 10.0))
        testPoints.add(GeoCoordinate(-12.086825, -77.019053, 10.0))
        testPoints.add(GeoCoordinate(-12.084889, -77.018650, 10.0))
        testPoints.add(GeoCoordinate(-12.084480, -77.019600, 10.0))
        testPoints.add(GeoCoordinate(-12.088697, -77.020372, 10.0))
        val geoPoligon = GeoPolygon(testPoints)

        val mapPolygon = MapPolygon(geoPoligon)
        mapPolygon.setLineWidth(8)
        mapPolygon.setFillColor(Color.TRANSPARENT)


        m_map?.addMapObject(mapPolygon);
    }

    private fun initMap() {
        this.m_mapFragment?.init(OnEngineInitListener { error ->
            if (error == OnEngineInitListener.Error.NONE) {
                m_map = m_mapFragment!!.getMap()


// Assume to select the 2nd map scheme in the available list
                m_map?.setMapScheme(Map.Scheme.TRUCK_DAY)
//                val features: EnumSet<FleetFeature> = m_map!!.getFleetFeaturesVisible()
//                features.add(FleetFeature.CONGESTION_ZONES)
//                features.add(FleetFeature.ENVIRONMENTAL_ZONES)
//                m_map?.setFleetFeaturesVisible(features)
//                m_map?.setFleetFeaturesVisible(EnumSet.of(Map.FleetFeature.TRUCK_RESTRICTIONS));

                if (m_map != null) {
                    //Centrado en el mapa con una ubicacion personalizada
                    m_map!!.setCenter(GeoCoordinate(-12.092831,
                            -77.032546), Map.Animation.NONE)

                    //Zoom en el mapa
                    m_map!!.setZoomLevel(14.0);

                    m_mapFragment?.mapGesture?.addOnGestureListener(MapListener(groupMarkerDetails!!, container_details!!, m_map!!), 0, false)

                }
            } else {
                println("ERROR: Cannot initialize AndroidXMapFragment")
            }
        })
    }


}


// Map gesture listener
private class MapListener (pGroup: Group, pContainerDetails: ConstraintLayout, pMap: Map) : MapGesture.OnGestureListener {

    var group: Group
    var containerDetails: ConstraintLayout
    var map: Map

    init{
        group = pGroup
        containerDetails = pContainerDetails
        map = pMap
    }

    override fun onLongPressRelease() {

    }

    override fun onRotateEvent(p0: Float): Boolean {
        return false
    }

    override fun onMultiFingerManipulationStart() {

    }

    override fun onPinchLocked() {

    }

    override fun onPinchZoomEvent(p0: Float, p1: PointF): Boolean {
        return false
    }

    override fun onTapEvent(p0: PointF): Boolean {
        return false
    }

    override fun onPanStart() {

    }

    override fun onMultiFingerManipulationEnd() {

    }

    override fun onDoubleTapEvent(p0: PointF): Boolean {
        return false
    }

    override fun onPanEnd() {

    }

    override fun onTiltEvent(p0: Float): Boolean {
        return false
    }

    override fun onMapObjectsSelected(list: MutableList<ViewObject>): Boolean {
        for (viewObject in list) {
            if (viewObject.baseType == ViewObject.Type.USER_OBJECT) {
                val mapObject = viewObject as MapObject
                if (mapObject.type == MapObject.Type.LABELED_MARKER) {
                    val window_marker = mapObject as MapMarker

                    val markerObj = window_marker.tag as CustomMarker
                    Log.d("Lugar seleccionado", markerObj.name)

                    group.visibility = View.VISIBLE
                    containerDetails.findViewById<TextView>(R.id.lblTitle).text  = markerObj.name
                    containerDetails.findViewById<TextView>(R.id.lblContactNumber).text  = markerObj.phoneNumber
                    containerDetails.findViewById<TextView>(R.id.lblAddress).text  = markerObj.address

                    map.setCenter(GeoCoordinate(markerObj.latitude,
                            markerObj.longitude), Map.Animation.BOW)

                    //Zoom en el mapa
                    map.setZoomLevel(14.0);
                    return false
                }
            }else{
                group.visibility = View.GONE
            }
        }
        return false
    }

    override fun onRotateLocked() {

    }

    override fun onLongPressEvent(p0: PointF): Boolean {
        return false
    }

    override fun onTwoFingerTapEvent(p0: PointF): Boolean {
        return false
    }

}


class CustomMarker(pAddress: String, pName: String, pPhoneNumber: String, pLatitude: Double, pLongitude: Double){
     var address: String

     var name: String

     var phoneNumber: String

     var latitude: Double

     var longitude: Double

    init {
        address = pAddress
        name = pName
        phoneNumber = pPhoneNumber
        latitude = pLatitude
        longitude = pLongitude
    }

}