package com.alicorp.pe.heredemo


import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import com.here.android.mpa.cluster.ClusterLayer
import com.here.android.mpa.common.*
import com.here.android.mpa.mapping.*
import com.here.android.mpa.mapping.Map
import java.io.Serializable
import java.lang.ref.WeakReference


class LocationActivity : AppCompatActivity() {

    private var mMapFragment: AndroidXMapFragment? = null
    private var mMap: Map? = null
    private var  groupMarkerDetails: Group? = null
    private var container_details: ConstraintLayout? = null
    private var destinationList = arrayListOf<ClientDestination>()
    private var lblTotalDestination: TextView? = null
    private var positioningManager: PositioningManager? = null
    private var myPositionMarker: MapMarker? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        var toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Entrega de pedidos"

        mMapFragment = supportFragmentManager.findFragmentById(R.id.mapfragment) as AndroidXMapFragment?
        lblTotalDestination = findViewById<TextView>(R.id.lbl_total_destination)
        groupMarkerDetails = findViewById(R.id.groupMarkerDetails)
        container_details = findViewById(R.id.container_details)
        initData()
        initMap()

    }

    fun initData(){
        destinationList.add(ClientDestination("Calle las Castañitas 123, San Isidro", "Centro Comercial 01", "+51 948663808", -12.092963, -77.021192))
        destinationList.add(ClientDestination("San Isidro 15036", "Centro Comercial 02", "+51 948663808", -12.093211, -77.020570))
        destinationList.add(ClientDestination("Av. República de Panamá 3071", "Centro Comercial 03", "+51 948663808", -12.093410, -77.021751))
        destinationList.add(ClientDestination("Av. Rivera Navarrete 2602-2692", "Centro Comercial 04", "+51 948663808", -12.089707, -77.027302))
        destinationList.add(ClientDestination("Calle Los Tulipanes 375", "Centro Comercial 05", "+51 948663808", -12.087872, -77.025604))
        destinationList.add(ClientDestination("Orquideas 675", "Centro Comercial 06", "+51 948663808", -12.090602, -77.028294))
        destinationList.add(ClientDestination("Jirón Soledad 458", "Centro Comercial 07", "+51 948663808", -12.090304, -77.031073))
        destinationList.add(ClientDestination("Calle Francisco Masias 2801-2717", "Centro Comercial 08", "+51 948663808", -12.090196, -77.025951))
        destinationList.add(ClientDestination("Jirón Oswaldo Hercelles 301-197", "Centro Comercial 09", "+51 948663808", -12.086828, -77.023513))
        destinationList.add(ClientDestination("Cercado de Lima 15034", "Centro Comercial 10", "+51 948663808", -12.087671, -77.020016))
        destinationList.add(ClientDestination("Francisco Almenara 248-282", "Centro Comercial 11", "+51 948663808", -12.085989, -77.023195))

        lblTotalDestination?.text = destinationList.size.toString()
    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.markers_forms, menu)
//        return true
//    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            R.id.actionMarkers -> showMarkers()
//            R.id.actionPolyline -> showPolyLines()
//        }
        return true
    }


    private fun initMap() {
        this.mMapFragment?.init(OnEngineInitListener { error ->
            if (error == OnEngineInitListener.Error.NONE) {
                mMap = mMapFragment!!.getMap()
                mMap?.mapScheme = Map.Scheme.TRUCK_DAY
                if (mMap != null) {
                    //Centrado en el mapa con una ubicacion personalizada
                    mMap!!.setCenter(GeoCoordinate(-12.092831,
                            -77.032546), Map.Animation.NONE)
                    //Zoom en el mapa
                    mMap!!.setZoomLevel(14.0);
                    mMapFragment?.mapGesture?.addOnGestureListener(MapGestureListener(groupMarkerDetails!!, container_details!!, mMap!!, howToArriveListener ), 0, false)
//                    mMapFragment?.positionIndicator?.isVisible = true
//                    mMapFragment?.positionIndicator?.setMarker(getMarkerImage(true))

                    mMap!!.isTrafficInfoVisible = true
                    Log.d("istrafficeenabled", mMap?.isTrafficInfoVisible.toString())
                    positioningManager = PositioningManager.getInstance()


                    initDestinationMarkers()
                    startListening()
                }
            } else {
                println("ERROR: Cannot initialize AndroidXMapFragment")
            }
        })
    }

    private fun initDestinationMarkers(){
        var icon = getMarkerImage(false)
        val cl = ClusterLayer()

        for ( (index, item) in destinationList.withIndex()){
            var markerCoordinate =  GeoCoordinate(item.latitude, item.longitude, 10.0);
            var marker = MapLabeledMarker(markerCoordinate, icon)
            marker.setLabelText("spa", item?.name)
            marker.tag = item
            if(index <= 3){
                cl.addMarker(marker)
            }else {
                mMap?.addMapObject(marker);
            }
        }

        mMap?.addClusterLayer(cl)
    }



    private fun getMarkerImage(isTruck: Boolean): Image{
        val icon = BitmapFactory.decodeResource(resources, if (isTruck)  R.drawable.ic_truck else  R.drawable.location)

        val image = Image()
        image.setBitmap(Bitmap.createScaledBitmap(icon, 80, 80, false))
        return image
    }


    override fun onResume() {
        startListening()
        super.onResume()
    }

    override fun onDestroy() {
        stopListening()
        super.onDestroy()
    }


    fun startListening(){
        if ( positioningManager != null ){
            positioningManager?.start(PositioningManager.LocationMethod.GPS)
            positioningManager?.addListener(WeakReference<PositioningManager.OnPositionChangedListener>(positionManagerListener))
        }
    }

    fun stopListening(){
        positioningManager?.stop()
        positioningManager?.removeListener(positionManagerListener)
        mMap = null
    }


    val howToArriveListener: MapGestureListener.HowToArriveListener = object : MapGestureListener.HowToArriveListener{
        override fun onHowToArriveClicked(destination: ClientDestination) {
            var intent = Intent(applicationContext, NavigationActivity::class.java).apply {
                putExtra("clientDestination", destination)
            }
            startActivity(intent)

        }
    }


    val positionManagerListener:  PositioningManager.OnPositionChangedListener = object : PositioningManager.OnPositionChangedListener{
        override fun onPositionFixChanged(p0: PositioningManager.LocationMethod?, p1: PositioningManager.LocationStatus?) {

        }

        override fun onPositionUpdated(p0: PositioningManager.LocationMethod?, p1: GeoPosition?, p2: Boolean) {
            Log.d("position update", p1?.coordinate?.latitude.toString())

            if (checkIfInMyZone(p1!!)) {
                if (myPositionMarker == null) {
                    var marker = MapMarker(p1!!.coordinate, getMarkerImage(true))
                    myPositionMarker = marker
                    mMap?.addMapObject(myPositionMarker!!)
                } else {
                    myPositionMarker?.coordinate = p1?.coordinate!!
                }
            }

        }
    }


    private fun checkIfInMyZone(receivedPosition: GeoPosition): Boolean{
        var center: android.location.Location = android.location.Location("")
        center.latitude = -12.078276
        center.longitude = -77.032630
        var test: android.location.Location = android.location.Location("")
        test.latitude = receivedPosition.coordinate.latitude
        test.longitude = receivedPosition.coordinate.longitude
        val distanceInMeters: Float = center.distanceTo(test)
        Log.d("Is in my position", (distanceInMeters < 100).toString())
        return distanceInMeters > 100
    }

    class ClientDestination(pAddress: String, pName: String, pPhoneNumber: String, pLatitude: Double, pLongitude: Double): Serializable{
        var address: String
        var name: String
        var phoneNumber: String
        var latitude: Double
        var longitude: Double

        init {
            address = pAddress
            name = pName
            phoneNumber = pPhoneNumber
            latitude = pLatitude
            longitude = pLongitude
        }
    }

    companion object {
        fun getIntent(poContext: Context) = Intent(poContext, LocationActivity::class.java)
    }
}


