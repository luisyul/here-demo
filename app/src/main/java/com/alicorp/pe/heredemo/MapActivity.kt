package com.alicorp.pe.heredemo

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.gson.Gson
import com.here.android.mpa.common.GeoBoundingBox
import com.here.android.mpa.common.GeoCoordinate
import com.here.android.mpa.common.OnEngineInitListener
import com.here.android.mpa.common.PositioningManager
import com.here.android.mpa.customlocation2.CLE2ProximityRequest
import com.here.android.mpa.customlocation2.CLE2Request.CLE2Error
import com.here.android.mpa.ftcr.FTCRRouteOptions
import com.here.android.mpa.ftcr.FTCRRoutePlan
import com.here.android.mpa.ftcr.FTCRRouter
import com.here.android.mpa.mapping.*
import com.here.android.mpa.mapping.Map
import com.here.android.mpa.mapping.Map.FleetFeature
import com.here.android.mpa.routing.RouteWaypoint
import com.here.android.mpa.routing.RoutingError
import java.util.*


class MapActivity : AppCompatActivity() {

    private var m_mapFragment: AndroidXMapFragment? = null
    private var m_map: Map? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setTitleTextColor(Color.WHITE)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Mapas"

        m_mapFragment = supportFragmentManager.findFragmentById(R.id.mapfragment) as AndroidXMapFragment?
        initMap()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.hello_map, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionSatelliteMode -> showSatelliteMode()
            R.id.actionPedestrianMode -> showPedestrianMode()
            R.id.actionNormalMode -> showNormalMode()
            R.id.actionNightMode -> showNightMode()
            R.id.actionTraffic -> showTraffic()
        }
        return true
    }

    private fun showSatelliteMode() {
        m_map?.mapScheme = Map.Scheme.SATELLITE_DAY
    }

    private fun showNormalMode() {
        m_map?.mapScheme = Map.Scheme.NORMAL_DAY
    }

    private fun showNightMode() {
        m_map?.mapScheme = Map.Scheme.NORMAL_NIGHT
    }

    private fun showTraffic() {
        m_map?.isTrafficInfoVisible = true
        m_map?.mapScheme = Map.Scheme.NORMAL_TRAFFIC_DAY
        val traffic: MapTrafficLayer = m_map!!.getMapTrafficLayer()
        traffic.displayFilter = TrafficEvent.Severity.VERY_HIGH
    }

    private fun showPedestrianMode() {
        m_map?.mapScheme = Map.Scheme.PEDESTRIAN_DAY_HYBRID
        m_map?.pedestrianFeaturesVisible = EnumSet.of(Map.PedestrianFeature.STAIRS,
            Map.PedestrianFeature.ESCALATOR, Map.PedestrianFeature.CROSSWALK,
            Map.PedestrianFeature.TUNNEL, Map.PedestrianFeature.BRIDGE)
    }

    fun myLocation(view: View) {
        val placesContainer: MapObject = MapContainer()
        val posManager: PositioningManager = PositioningManager.getInstance()

        posManager.start(PositioningManager.LocationMethod.GPS_NETWORK)

        val position = posManager.position
        val cor = position.coordinate

        // Set a pedestrian friendly map scheme
        m_map?.mapScheme = Map.Scheme.PEDESTRIAN_DAY

        // Display position indicator
        m_map?.positionIndicator?.isVisible = true

        m_map?.addMapObject(placesContainer)

        // Set the map center coordinate to the current position
        m_map?.setCenter(GeoCoordinate(posManager.position.coordinate.latitude,
            posManager.position.coordinate.longitude), Map.Animation.LINEAR)
        m_map?.zoomLevel = 14.0
    }

    private fun showFleetMode() {
        m_map?.mapScheme = Map.Scheme.TRUCK_DAY
        val features: EnumSet<FleetFeature> = m_map!!.fleetFeaturesVisible
        features.add(FleetFeature.CONGESTION_ZONES)
        features.add(FleetFeature.ENVIRONMENTAL_ZONES)
        m_map?.fleetFeaturesVisible = features
    }

    private fun initMap() {
        this.m_mapFragment?.init { error ->
            if (error == OnEngineInitListener.Error.NONE) {
                m_map = m_mapFragment!!.map

                if (m_map != null) {
                    //Centrado en el mapa con una ubicacion personalizada
                    showFleetMode()
                    //m_map?.setCenter(GeoCoordinate(-12.092831, -77.032546), Map.Animation.NONE)
                    m_map?.setCenter(GeoCoordinate(-12.093676, -77.025797), Map.Animation.NONE)
                    //Zoom en el mapa
                    m_map?.zoomLevel = 14.0

                    m_mapFragment!!.mapGesture?.addOnGestureListener(MyGestureListener(this), 100, true)
                    calculateRoute()
                    checkProximity()
                }
            } else {
                println("ERROR: Cannot initialize AndroidXMapFragment")
            }
        }


    }

    override fun onPause() {
        super.onPause()
        m_mapFragment?.onPause()
    }

    override fun onResume() {
        super.onResume()
        m_mapFragment?.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        m_mapFragment?.onDestroy()
    }

    private fun calculateRoute() {
        // waypoints setup
        val start = RouteWaypoint(GeoCoordinate(52.514184, 13.316419))
        val destination = RouteWaypoint(GeoCoordinate(52.512272, 13.383096))
        val waypoints = Arrays.asList(start, destination)

        // fleet telematics options
        val routeOptions = FTCRRouteOptions()
        routeOptions.setUseTraffic(true) // Other transport modes such as TRUCK, PEDESTRIAN, SCOOTER, BICYCLE
            // and BUS are also supported.
            .setTransportMode(FTCRRouteOptions.TransportMode.CAR)
            .setRouteType(FTCRRouteOptions.Type.FASTEST) // see API reference to check all possible routing options
            .addAvoidArea(
                GeoBoundingBox(
                    GeoCoordinate(52.521842, 13.375375),
                    GeoCoordinate(52.518212, 13.380335)
                )
            )
        val router = FTCRRouter()
        val ftcrRoutePlan = FTCRRoutePlan(waypoints, routeOptions)
        // set overlay name if needed
        ftcrRoutePlan.overlay = "OVERLAYNAME"
        router.calculateRoute(ftcrRoutePlan) { routes, error ->
            if (error.errorCode == RoutingError.NONE && routes.isNotEmpty()) {
               // startNavigation(routes[0])
            }
        }
    }

    private fun checkProximity() {
        val layerIds = arrayListOf<String>()
        layerIds.add("ALICORP_CLIENTS")
        val radius = 8 // 8 m

        val location = GeoCoordinate(-12.093676, -77.025797)
        val req = CLE2ProximityRequest(layerIds, location, radius)

        req.execute { result, error -> // if CLE2Error.NONE.equals(error) is true, the request was successful
            Log.e("error custom loc", Gson().toJson(error))
            if (error == CLE2Error.NONE) {
                val gemetry = result!!.geometries
                for (geo in gemetry) {
                    val attributeMap = geo.attributes
                    val name = attributeMap["NAME1"]
                    val distance = geo.distance
                    Log.e("geoo", Gson().toJson(geo))
                }
            }
        }
    }
}