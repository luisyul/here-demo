package com.alicorp.pe.heredemo


import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.here.android.mpa.common.*
import com.here.android.mpa.guidance.NavigationManager
import com.here.android.mpa.mapping.*
import com.here.android.mpa.mapping.Map
import com.here.android.mpa.routing.*
import java.lang.ref.WeakReference


class NavigationActivity : AppCompatActivity() {

    private var mMapFragment: AndroidXMapFragment? = null
    private var mMap: Map? = null
    private var positioningManager: PositioningManager? = null
    private var clientDestination: LocationActivity.ClientDestination? = null
    private var currentLocation: GeoPosition? = null
    private var mapRoute: MapRoute? = null
    private var lblETA: TextView? = null
    private var myPositionMarker: MapMarker? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        var toolbar = findViewById<Toolbar>(R.id.toolbar)

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Entrega de pedidos"

        if(intent.hasExtra("clientDestination")){
            clientDestination = intent.getSerializableExtra("clientDestination") as LocationActivity.ClientDestination
        }

        lblETA = findViewById<TextView>(R.id.lbl_eta)
        mMapFragment = supportFragmentManager.findFragmentById(R.id.mapfragment) as AndroidXMapFragment?
        initMap()

    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            R.id.actionMarkers -> showMarkers()
//            R.id.actionPolyline -> showPolyLines()
//        }
        return true
    }


    private fun initMap() {
        this.mMapFragment?.init(OnEngineInitListener { error ->
            if (error == OnEngineInitListener.Error.NONE) {
                mMap = mMapFragment!!.getMap()
                mMap?.mapScheme = Map.Scheme.NORMAL_DAY
                if (mMap != null) {
                    //Centrado en el mapa con una ubicacion personalizada
                    mMap!!.setCenter(GeoCoordinate(-12.092831,
                            -77.032546), Map.Animation.NONE)
                    //Zoom en el mapa
                    mMap!!.setZoomLevel(14.0);
//                    mMapFragment?.positionIndicator?.isVisible = true
//                    mMapFragment?.positionIndicator?.setMarker(getMarkerImage(true))

                    mMap!!.isTrafficInfoVisible =  true
                    mMap!!.mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.INCIDENT, true);
                    mMap!!.mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.FLOW, true);
                    Log.d("istrafficeenabled", mMap?.isTrafficInfoVisible.toString())

                    positioningManager = PositioningManager.getInstance()

                    initDestinationMarkers()
                    startListening()

                }
            } else {
                println("ERROR: Cannot initialize AndroidXMapFragment")
            }
        })
    }




    private fun initDestinationMarkers(){
        var icon = getMarkerImage(false)

        var markerCoordinate =  GeoCoordinate(clientDestination!!.latitude, clientDestination!!.longitude, 10.0);
        var marker = MapLabeledMarker(markerCoordinate, icon)
        marker.setLabelText("spa", clientDestination?.name)
        marker.tag = clientDestination
        mMap?.addMapObject(marker);

    }



    private fun getMarkerImage(isTruck: Boolean): Image{
        val icon = BitmapFactory.decodeResource(resources, if (isTruck)  R.drawable.ic_truck else  R.drawable.location)

        val image = Image()
        image.setBitmap(Bitmap.createScaledBitmap(icon, 80, 80, false))
        return image
    }


    private fun calculateRoute(){
        val router = CoreRouter()
        val routePlan = RoutePlan()
        routePlan.addWaypoint(RouteWaypoint(GeoCoordinate(currentLocation!!.coordinate.latitude, currentLocation!!.coordinate.longitude)))
        routePlan.addWaypoint(RouteWaypoint(GeoCoordinate(clientDestination!!.latitude, clientDestination!!.longitude)))

        val routeOptions = RouteOptions()
        routeOptions.transportMode = RouteOptions.TransportMode.CAR
        routeOptions.routeType = RouteOptions.Type.FASTEST

        routePlan.routeOptions = routeOptions

        router.calculateRoute(routePlan, routeListener)
    }

    override fun onResume() {
        startListening()
        super.onResume()
    }

    override fun onDestroy() {
        stopListening()
        super.onDestroy()
    }


    fun startListening(){
        if ( positioningManager != null ){
            positioningManager?.start(PositioningManager.LocationMethod.GPS)
            positioningManager?.addListener(WeakReference<PositioningManager.OnPositionChangedListener>(positionManagerListener))
        }
    }

    fun stopListening(){
        positioningManager?.stop()
        positioningManager?.removeListener(positionManagerListener)
        mMap = null
    }


    val positionManagerListener:  PositioningManager.OnPositionChangedListener = object : PositioningManager.OnPositionChangedListener{
        override fun onPositionFixChanged(p0: PositioningManager.LocationMethod?, p1: PositioningManager.LocationStatus?) {

        }

        override fun onPositionUpdated(p0: PositioningManager.LocationMethod?, p1: GeoPosition?, p2: Boolean) {


            Log.d("position update", p1?.coordinate?.latitude.toString())
            if (checkIfInMyZone(p1!!)) {
                currentLocation = p1
                calculateRoute()
                if (myPositionMarker == null) {
                    var marker = MapMarker(p1!!.coordinate, getMarkerImage(true))
                    myPositionMarker = marker
                    mMap?.addMapObject(myPositionMarker!!)
                } else {
                    myPositionMarker?.coordinate = p1?.coordinate!!
                }
            }
        }
    }


    private fun checkIfInMyZone(receivedPosition: GeoPosition): Boolean{
        var center: android.location.Location = android.location.Location("")
        center.latitude = -12.078276
        center.longitude = -77.032630
        var test: android.location.Location = android.location.Location("")
        test.latitude = receivedPosition.coordinate.latitude
        test.longitude = receivedPosition.coordinate.longitude
        val distanceInMeters: Float = center.distanceTo(test)
        Log.d("Is in my position", (distanceInMeters < 100).toString())
        return distanceInMeters > 100
    }

    val routeListener = object: CoreRouter.Listener{
        override fun onCalculateRouteFinished(routeResult: MutableList<RouteResult>, error: RoutingError) {
            if (error == RoutingError.NONE) {
                if(mapRoute != null) {
                    mapRoute!!.setRoute(routeResult[0].route)

                }else{
                    mapRoute = MapRoute(routeResult[0].route);
                    mMap?.addMapObject(mapRoute!!);
                }

                // Render the route on the map

                val timeInSeconds: Int? = routeResult[0].route.getTtaIncludingTraffic(Route.WHOLE_ROUTE)?.duration
                lblETA?.text = getDurationString(timeInSeconds!!)


//
//                val navigationManager = NavigationManager.getInstance()
//                navigationManager.setMap(mMap);
            }
            else {
                // Display a message indicating route calculation failure
            }
        }

        override fun onProgress(p0: Int) {

        }
    }


    private fun getDurationString(seconds: Int): String? {
        var seconds = seconds
        val hours = seconds / 3600
        val minutes = seconds % 3600 / 60
        seconds = seconds % 60
        return twoDigitString(hours) + " : " + twoDigitString(minutes) + " : " + twoDigitString(seconds)
    }

    private fun twoDigitString(number: Int): String {
        if (number == 0) {
            return "00"
        }
        return if (number / 10 == 0) {
            "0$number"
        } else number.toString()
    }

}


