package com.alicorp.pe.heredemo

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    private var permissionsRequestor: PermissionsRequestor? = null
    private var sharedText = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        handlePermissions()

        when (intent?.action) {
            Intent.ACTION_SEND -> {
                if ("text/plain" == intent.type) {
                    handleSendText(intent)
                }
            }
        }
    }

    private fun handleSendText(intent: Intent) {
        val sharedText = intent.getStringExtra(Intent.EXTRA_TEXT)
        if (sharedText != null) {
            this.sharedText = sharedText
            if(sharedText == "demo")
                startActivity(LocationActivity.getIntent(this@MainActivity))
        }
    }

    fun handlePermissions(){
        permissionsRequestor = PermissionsRequestor(this)
        permissionsRequestor?.request(resultListener)
    }

    fun goToHelloMap(view: View) {
        startActivity(Intent(this, MapActivity::class.java))
    }

    fun goToMarcadoresFormas(view: View) {
        startActivity(Intent(this, MarkersForms::class.java))
    }

    fun goToLocation(view: View) {
        startActivity(Intent(this, LocationActivity::class.java))
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        permissionsRequestor!!.onRequestPermissionsResult(requestCode, grantResults)
    }

    val resultListener  = object : PermissionsRequestor.ResultListener{
        override fun permissionsGranted() {
            if(sharedText == "demo")
                startActivity(LocationActivity.getIntent(this@MainActivity))
        }

        override fun permissionsDenied() {

        }
    }

}