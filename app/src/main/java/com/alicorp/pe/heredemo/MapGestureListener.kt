package com.alicorp.pe.heredemo

import android.graphics.PointF
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import com.here.android.mpa.common.GeoCoordinate
import com.here.android.mpa.common.ViewObject
import com.here.android.mpa.mapping.*
import com.here.android.mpa.mapping.Map


class MapGestureListener (pGroup: Group, pContainerDetails: ConstraintLayout, pMap: Map, pCallback: HowToArriveListener) : MapGesture.OnGestureListener {

    var callback: HowToArriveListener
    var group: Group
    var containerDetails: ConstraintLayout
    var map: Map

    init{
        group = pGroup
        containerDetails = pContainerDetails
        map = pMap
        callback = pCallback
    }

    override fun onLongPressRelease() {

    }

    override fun onRotateEvent(p0: Float): Boolean {
        return false
    }

    override fun onMultiFingerManipulationStart() {

    }

    override fun onPinchLocked() {

    }

    override fun onPinchZoomEvent(p0: Float, p1: PointF): Boolean {
        return false
    }

    override fun onTapEvent(p0: PointF): Boolean {
        return false
    }

    override fun onPanStart() {

    }

    override fun onMultiFingerManipulationEnd() {

    }

    override fun onDoubleTapEvent(p0: PointF): Boolean {
        return false
    }

    override fun onPanEnd() {

    }

    override fun onTiltEvent(p0: Float): Boolean {
        return false
    }

    override fun onMapObjectsSelected(list: MutableList<ViewObject>): Boolean {
        for (viewObject in list) {
            if (viewObject.baseType == ViewObject.Type.USER_OBJECT) {
                val mapObject = viewObject as MapObject
                if (mapObject.type == MapObject.Type.LABELED_MARKER) {
                    val window_marker = mapObject as MapMarker

                    val markerObj = window_marker.tag as LocationActivity.ClientDestination
                    Log.d("Lugar seleccionado", markerObj.name)

                    group.visibility = View.VISIBLE
                    containerDetails.findViewById<TextView>(R.id.lblTitle).text  = markerObj.name
                    containerDetails.findViewById<TextView>(R.id.lblContactNumber).text  = markerObj.phoneNumber
                    containerDetails.findViewById<TextView>(R.id.lblAddress).text  = markerObj.address
                    containerDetails.findViewById<ImageView>(R.id.btnClose).setOnClickListener(View.OnClickListener {
                        group.visibility = View.GONE

                    })
                    containerDetails.findViewById<Button>(R.id.btnHowToArrive).setOnClickListener(View.OnClickListener {
                        callback.onHowToArriveClicked(markerObj)
                    })


                    map.setCenter(GeoCoordinate(markerObj.latitude,
                            markerObj.longitude), Map.Animation.NONE)

                    //Zoom en el mapa
                    map.setZoomLevel(16.0);
                    return false
                }
            }else{
                group.visibility = View.GONE
            }
        }
        return false
    }

    override fun onRotateLocked() {

    }

    override fun onLongPressEvent(p0: PointF): Boolean {
        return false
    }

    override fun onTwoFingerTapEvent(p0: PointF): Boolean {
        return false
    }


    interface  HowToArriveListener {
        fun onHowToArriveClicked(destination: LocationActivity.ClientDestination)
    }

}